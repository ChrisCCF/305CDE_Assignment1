var notes = [];

/*
 * displays the 'add' screen if this has been bookmarked by user
 */
if (window.location.hash == '#add' || notes.length === 0) {
	document.getElementById('editPage').style.display = 'none';
} else {
	document.getElementById('addPage').style.display = 'none';
}

document.querySelector('#save').onclick = function() {
	console.log('add note');
	var username_t = document.getElementById("username").value;
	var email_t = document.getElementById("email").value;
	var age_t = document.getElementById("age").value;
	var person = {username: username_t, email: email_t, age: age_t};
	notes.push(person);
	console.log('add finish');
	document.getElementById("username").value = ''
	document.getElementById("name").value = ''
	document.getElementById("password").value = ''
	document.getElementById("email").value = ''
	document.getElementById("age").value = ''
};

/*
 * handles navigation between the add and edit 'screens'
 */ 
document.querySelector('nav > ul > li:nth-child(1)').onclick = function() {
	console.log('first link clicked');
	document.getElementById('addPage').style.display = 'block';
	document.getElementById('editPage').style.display = 'none';
};

document.querySelector('nav > ul > li:nth-child(2)').onclick = function() {
	console.log('second link clicked');
	document.getElementById('editPage').style.display = 'block';
	document.getElementById('addPage').style.display = 'none'
	var table = document.getElementById('list');
	table.innerHTML = '';
	for (var i=0; i<notes.length; i++) {
		console.log(notes[i].username);
		var row = document.createElement('tr');
		var number = i+1
		row.id = i;
		row.innerHTML = '<td>'+number+'. '+notes[i].username+'</a></td><td>   email: '+notes[i].email+'</td>'+'<td>age:'+ notes[i].age + '</td>';
		table.appendChild(row);
	}
};

document.querySelector('#check').onclick = function() {
	var total =0
	var counter =1
	for (var i=0; i<notes.length; i++) {
		var now = Number(notes[i].age)
		total = total + now
		counter = counter + i
	}
	var total_avg = total/counter
	alert("The average age is " + total_avg);
	console.log(total_avg);
};
